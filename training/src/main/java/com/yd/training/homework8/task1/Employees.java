package com.yd.training.homework8.task1;

import java.io.*;
import java.util.*;

public class Employees extends EmployeeManager{

    private static final String FILE_NAME = "employees.dat";

    public static void main(String[] args) {
        File file = new File(FILE_NAME);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.out.println("There was a problem creating a file. " + e.toString());
            }
        }

        TreeMap<Integer, Employee> table = new TreeMap<Integer, Employee>();
        try {
            FileInputStream fileStream = new FileInputStream(FILE_NAME);
            ObjectInputStream inputStream = new ObjectInputStream(fileStream);
            table.putAll((TreeMap<Integer, Employee>) inputStream.readObject());
            inputStream.close();
            fileStream.close();
            System.out.println("Employees data was successfully loaded from the file.");
        } catch (Exception e) {
            System.out.println("There was a problem reading data from the file. " + e.toString());
        }
        boolean repeat = true;
        Scanner reader = new Scanner(System.in);
        while (repeat) {
            System.out.println("Enter a command\n1 - Add an employee;\n2 - Delete an employee;\n3 - Print employees table;\n4 - Exit.");
            int command = reader.nextInt();
            switch (command) {
                case 1:
                    addEmployee(table, reader);
                    break;
                case 2:
                    removeEmployee(table, reader);
                    break;
                case 3:
                    printEmployeeTable(table);
                    break;
                case 4:
                    repeat = false;
                    break;
                default:
                    System.out.println("Invalid input.");
            }
        }
        reader.close();
        try {
            FileOutputStream fileStream = new FileOutputStream(FILE_NAME);
            ObjectOutputStream outputStream = new ObjectOutputStream(fileStream);
            outputStream.writeObject(table);
            outputStream.close();
            fileStream.close();
            System.out.println("Employees data was successfully saved to the file.");
        } catch (Exception e) {
            System.out.println("There was a problem saving data to the file. " + e.toString());
        }
    }
}

