package com.yd.training.homework8.task1;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class EmployeeManager {

    public static void addEmployee(TreeMap<Integer, Employee> table, Scanner reader) {
        Integer id = 1;
        if (table.size() != 0) {
            id = table.lastKey() + 1;
        }
        System.out.println("Enter employee first name:");
        String firstName = reader.next();
        System.out.println("Enter employee last name:");
        String lastName = reader.next();
        Employee employee = new Employee(id, firstName, lastName);
        try {
            table.put(id, employee);
        } catch (Exception e) {
            System.out.println(String.format("Cannot add employee with ID: %d to the table.", id));
        }
    }

    public static void removeEmployee(TreeMap<Integer, Employee> table, Scanner reader) {
        System.out.println("Enter employee ID to remove:");
        int employeeId = reader.nextInt();
        if (table.containsKey(employeeId)) {
            table.remove(employeeId);
        } else {
            System.out.println(String.format("Employee with ID: %d doesn't exist.", employeeId));
        }
    }

    public static void printEmployeeTable(TreeMap<Integer, Employee> table) {
        for (Map.Entry<Integer, Employee> item : table.entrySet()) {
            System.out.println(item.getValue());
        }
    }
}
