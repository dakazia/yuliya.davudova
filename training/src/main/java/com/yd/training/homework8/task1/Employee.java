package com.yd.training.homework8.task1;

import java.io.Serializable;
import java.security.InvalidParameterException;

public class Employee implements Serializable{

    private Integer id;
    private String firstName;
    private String lastName;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Employee(Integer id, String firstName, String lastName) {
        if (firstName == null || lastName == null) {
            throw new InvalidParameterException();
        }
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Employee() {
        this(0, "", "");
    }

    @Override
    public String toString() {
        return String.format("%d: %s %s", this.id, this.firstName, this.lastName);
    }
}

