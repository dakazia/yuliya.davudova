package com.yd.training.homework6;

import java.security.InvalidParameterException;

public class ArrayWrapper<T> {

    private final T[] array;

    public ArrayWrapper(T[] array){
        if(array == null){
            throw new InvalidParameterException();
        }

        this.array = array;
    }

    public T get(int index){
        if (index < 1 || index > this.array.length){
            throw new IncorrectArrayWrapperIndex(index);
        }

        return this.array[index - 1];
    }

    public boolean replace(int index, T value){
        if (index < 1 || index > this.array.length){
            throw new IncorrectArrayWrapperIndex(index);
        }

        if(value instanceof Integer &&
                !checkValue(Integer.valueOf(value.toString()),
                        Integer.valueOf(this.array[index - 1].toString()))){
            return false;
        }

        if(value instanceof String &&
                !checkValue(value.toString(), this.array[index - 1].toString())){
            return false;
        }

        this.array[index - 1] = value;

        return true;
    }

    private static boolean checkValue(int value, int arrayValue){
        return value > arrayValue;
    }

    private static boolean checkValue(String value, String arrayValue){
        return value.length()>arrayValue.length();
    }
}

