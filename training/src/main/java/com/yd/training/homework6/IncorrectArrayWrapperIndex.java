package com.yd.training.homework6;

public class IncorrectArrayWrapperIndex  extends RuntimeException {

    static final long serialVersionUID = 1;
    private final int index;

    public IncorrectArrayWrapperIndex(int index){
        this.index = index;
    }

    public String toString(){
        return "Index " + this.index + " is out of range.";
    }
}
