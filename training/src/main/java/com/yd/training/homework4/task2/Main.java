package com.yd.training.homework4.task2;

public class Main {

    public static void main(String args[]) {

        int[] array = new int[]{21, 25, 31, 7, 35, 8, 50};
        SortingContex context = new SortingContex(1);
        System.out.print("\nSorting with Bubble sort.\n");
        context.sort(array);
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + "  ");
        }
        array = new int[]{18, 2, 1, 55, 12, 10, 3};
        context = new SortingContex(2);
        System.out.print("\nSorting with Selection sort.\n");
        context.sort(array);

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + "  ");
        }
    }
}
