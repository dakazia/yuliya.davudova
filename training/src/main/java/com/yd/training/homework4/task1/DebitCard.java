package com.yd.training.homework4.task1;

public class DebitCard extends Card{

    public DebitCard(String cardholderName){
        super(cardholderName);
    }

    public DebitCard(String cardholderName, double balance){
        super(cardholderName, balance);
    }

    @Override
    public double withdrawCash(double withdrawAmount){
        if(withdrawAmount > this.balance){
            throw new IllegalArgumentException("Withdraw amount cannot be greater than available balance.");
        }
        return super.withdrawCash(withdrawAmount);
    }
}
