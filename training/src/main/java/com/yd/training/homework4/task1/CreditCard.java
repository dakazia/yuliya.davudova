package com.yd.training.homework4.task1;

public class CreditCard extends Card{

    public CreditCard(String cardholderName){
        super(cardholderName);
    }

    public CreditCard(String cardholderName, double balance){
        super(cardholderName, balance);
    }
}