package com.yd.training.homework4.task1;

public class Atm {

    private Card card;

    public Atm() {
        this.card = null;
    }

    public Atm(Card card) {
        this.card = card;
    }

    public void insertCard(Card card) {
        if (this.card != null) {
            throw new IllegalArgumentException("Card already inserted.");
        }
        this.card = card;
    }

    public void removeCard(Card card) {
        if (this.card == null) {
            throw new IllegalStateException("There is no card in this ATM. Please, insert card first.");
        }
        this.card = null;
    }

    public double depositCash(double depositAmount) {
        if (this.card == null) {
            throw new IllegalStateException("There is no card in this ATM. Please, insert card first.");
        }
        return this.card.depositCash(depositAmount);
    }

    public double withdrawCash(double withdrawAmount) {
        if (this.card == null) {
            throw new IllegalStateException("There is no card in this ATM. Pleae, insert card first.");
        }
        return this.card.withdrawCash(withdrawAmount);
    }

    public double getCardBalance() {
        if (this.card == null) {
            throw new IllegalStateException("There is no card in this ATM. Please, insert card first.");
        }
        return this.card.getBalance();
    }
}
