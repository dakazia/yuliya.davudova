package com.yd.training.homework4.task2;

public class SortingContex {

    private Sorter sorter = null;

    public SortingContex(int sorter) {
        switch (sorter) {
            case 1:
                this.sorter = new BubbleSort();
                break;
            case 2:
                this.sorter = new SelectionSort();
                break;
            default:
                throw new IllegalArgumentException();
        }
    }

    public void sort(int[] array) {
        sorter.sort(array);
    }
}
