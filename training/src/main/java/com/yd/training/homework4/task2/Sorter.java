package com.yd.training.homework4.task2;

public abstract class Sorter {

    public abstract void sort(int[] array);
}
