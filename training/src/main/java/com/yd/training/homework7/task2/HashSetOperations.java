package com.yd.training.homework7.task2;
import java.util.*;
import java.security.InvalidParameterException;


public class HashSetOperations {

    public static void main(String[] args) {
        Integer[] a = {101, 102, 103, 104, 105, 106};
        Integer[] b = {105, 106, 107, 108, 109, 110};

        HashSet<Integer> firstDigits = new HashSet(Arrays.asList(a));
        HashSet<Integer> secondDigits = new HashSet(Arrays.asList(b));
        HashSet<Integer> result;
        result = SetOperations.union(firstDigits, secondDigits);
        System.out.println(result);
        result = SetOperations.intersection(firstDigits, secondDigits);
        System.out.println(result);
        result = SetOperations.difference(firstDigits, secondDigits);
        System.out.println(result);
        result = SetOperations.symmetricDifference(firstDigits, secondDigits);
        System.out.println(result);
    }
}

class SetOperations {

    public static HashSet union(HashSet a, HashSet b) {
        if (a == null || b == null) {
            throw new InvalidParameterException();
        }
        HashSet<Integer> result = new HashSet(a);
        result.addAll(b);
        return result;
    }

    public static HashSet intersection(HashSet a, HashSet b) {
        if (a == null || b == null) {
            throw new InvalidParameterException();
        }
        HashSet<Integer> result = new HashSet(a);
        result.retainAll(b);
        return result;
    }

    public static HashSet difference(HashSet a, HashSet b) {
        if (a == null || b == null) {
            throw new InvalidParameterException();
        }
        HashSet<Integer>  result = new HashSet(a);
        result.removeAll(b);
        return result;
    }

    public static HashSet symmetricDifference(HashSet a, HashSet b) {
        if (a == null || b == null) {
            throw new InvalidParameterException();
        }
        HashSet<Integer>  result = union(a, b);
        result.removeAll(intersection(a, b));
        return result;
    }
}

