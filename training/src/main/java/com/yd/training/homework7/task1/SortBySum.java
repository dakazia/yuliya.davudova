package com.yd.training.homework7.task1;

import java.util.Comparator;
import java.util.Arrays;

public class SortBySum {

    public static void main(String[] args) {
        Integer [] array = new Integer[] { 31, 12, 51, 45, 28, 16 };
        Arrays.sort(array, new DigitSumComparator());
        System.out.println(Arrays.toString(array));
    }
}

class DigitSumComparator implements Comparator<Number> {
    @Override
    public int compare(Number a, Number b){
        return getDigitsSum(a) - getDigitsSum(b);
    }

    private static int getDigitsSum(Number n){
        int sum = 0;
        char[] digits = n.toString().toCharArray();
        for (int i = 0; i < digits.length; i++) {
            if (Character.isDigit(digits[i])) {
                sum += Character.getNumericValue(digits[i]);
            }
        }

        return sum;
    }
}

