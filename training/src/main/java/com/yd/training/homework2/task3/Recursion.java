package com.yd.training.homework2.task3;

public class Recursion {

    public static void main(String[] args) {
        calculateIntFibonacci();
        calculateLongFibonacci();
    }

    public static void calculateLongFibonacci() {
        int i = 0;
        long previousResult = 0;

        try {
            while (true) {
                long currentResult = calculateLongFibonacciRecursion(1, 1, ++i);
                if (currentResult < previousResult) {
                    break;
                }

                previousResult = currentResult;
            }
        } catch (StackOverflowError e) {
        }

        System.out.println("The number of the maximum value of the Fibonacci sequence for the type long is " + --i + " ");
    }

    private static long calculateLongFibonacciRecursion(long a, long b, int n) {
        if (n == 1) {
            return a;
        }

        if (n == 2) {
            return b;
        }

        return calculateLongFibonacciRecursion(b, a + b, n - 1);
    }

    public static void calculateIntFibonacci() {
        int i = 0;
        int previousResult = 0;

        try {
            while (true) {
                int currentResult = calculateIntFibonacciRecursion(1, 1, ++i);
                if (currentResult < previousResult) {
                    break;
                }

                previousResult = currentResult;
            }
        } catch (StackOverflowError e) {
        }

        System.out.println("The number of the maximum value of the Fibonacci sequence for the type int is " + --i + " ");
    }

    private static int calculateIntFibonacciRecursion(int a, int b, int n) {
        if (n == 1) {
            return a;
        }

        if (n == 2) {
            return b;
        }

        return calculateIntFibonacciRecursion(b, a + b, n - 1);
    }
}