package com.yd.training.homework2.task1;

public class Initializer {

    static String st;
    static byte b;
    static short s ;
    static char c;
    static int i;
    static long l;
    static float f;
    static double d;
    static boolean bool;

    static Initializer aInitializer;

    public static void main(String[] args) {
        System.out.println("Default String is " + st);
        System.out.println("Default byte is " + b);
        System.out.println("Default short is " + s);
        System.out.println("Default char is " + c);
        System.out.println("Default int is " + i);
        System.out.println("Default long is " + l);
        System.out.println("Default float is " + f);
        System.out.println("Default double is " + d);
        System.out.println("Default boolean is " + bool);

        System.out.println("Default Initializer is " + aInitializer);
    }
}
