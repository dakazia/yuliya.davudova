package com.yd.training.homework2.task2;

public class OrderOfPriority  {

    static int staticVariable;
    int nonStaticVariable;

    static {
        System.out.println("The first static initialization block.");
        staticVariable = 10;
        System.out.println("The staticVariable is initialized with value " + staticVariable);
    }

    static {
        System.out.println("The second static initialization block.");
    }

    {
        System.out.println("The first instance initialization block.");
        nonStaticVariable = 20;
        System.out.println("The nonStaticVariable is initialized with value " + nonStaticVariable);
    }

    {
        System.out.println("The second instance initialization block.");
    }

    public OrderOfPriority (){
        System.out.println("The constructor.");
    }

    public static void main(String[] args) {
        System.out.println("The main() method.");
        OrderOfPriority  mainVariable;
        System.out.println("The mainVariable is declared.");
        mainVariable = new OrderOfPriority ();
        System.out.println("The mainVariable is initialized with a new object.");
    }
}
