package com.yd.training.homework3.task2;

import java.util.Arrays;

public class Median {

    public static float median(int array[]) {
        Arrays.sort(array);
        int middle = array.length / 2;
        if (array.length % 2 == 0) {
            return (float) (array[middle - 1] + array[middle]) / 2;
        } else {
            return array[middle];
        }
    }

    public static double median(double array[]) {
        Arrays.sort(array);
        int middle = array.length / 2;
        if (array.length % 2 == 0) {
            return (array[middle - 1] + array[middle]) / 2;
        } else {
            return array[middle];
        }
    }
}
