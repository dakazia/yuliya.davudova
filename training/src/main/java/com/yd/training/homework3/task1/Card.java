package com.yd.training.homework3.task1;

public class Card {

    private String cardholderName;
    private double balance;

    public Card(String cardholderName) {
        this(cardholderName, 0.0);
    }

    public Card(String cardholderName, double balance) {
        this.cardholderName = cardholderName;
        this.balance = balance;
    }

    public double getBalance() {
        return this.balance;
    }

    public String getCardholderName() {
        return this.cardholderName;
    }

    public double depositCash(double depositAmount) {
        if (depositAmount <= 0) {
            throw new IllegalArgumentException("Deposit amount cannot be negative or zero.");
        }
        this.balance += depositAmount;
        return this.balance;
    }

    public double withdrawCash(double withdrawAmount) {
        if (withdrawAmount <= 0) {
            throw new IllegalArgumentException("Withdraw amount cannot be negative or zero.");
        }
        if (withdrawAmount > this.balance) {
            throw new IllegalArgumentException("Withdraw amount cannot be greater than available balance.");
        }
        this.balance -= withdrawAmount;
        return this.balance;
    }

    public double convertBalance(double rateOfConvertion) {
        if (rateOfConvertion <= 0) {
            throw new IllegalArgumentException("Rate of convertion cannot be negative or zero.");
        }
        return this.balance * rateOfConvertion;
    }
}
