package com.yd.training.homework1.task1;

public class CountFormula {

    public static void main(String[] args) {
        int a;
        int p;
        double m1;
        double m2;
        double G;

        if (args == null || args.length < 4) {
            System.out.println("Invalid number of arguments");
            System.exit(0);

        } else {

             try {
                a = Integer.parseInt(args[0]);
                p = Integer.parseInt(args[1]);
                m1 = Double.parseDouble(args[2]);
                m2 = Double.parseDouble(args[3]);
                G=4*Math.pow(Math.PI,2)*(Math.pow(a ,3)/(Math.pow(p,2)*(m1+m2)));

                if (G == Double.POSITIVE_INFINITY || G == Double.NEGATIVE_INFINITY) {

                    throw new ArithmeticException();
                }
                System.out.println("Reply: "+G);

             }  catch (NumberFormatException e) {

                    System.err.println("One of the arguments is not a number.");

             } catch (ArithmeticException e) {

                    System.out.println("Error. Division by zero.");

             }
        }
    }
}
