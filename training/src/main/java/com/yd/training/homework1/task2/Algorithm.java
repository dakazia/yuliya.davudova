package com.yd.training.homework1.task2;

public class Algorithm {

    private static final byte LOOP_WHILE = 1;
    private static final byte LOOP_DO_WHILE = 2;
    private static final byte LOOP_FOR = 3;

    private static final byte ALG_FIBONACCI = 1;
    private static final byte ALG_FACTORIAL = 2;

    public static void main(String[] args) {

        byte algorithmId;
        byte loopType;
        int n;

        algorithmId = Byte.parseByte(args[0]);
        loopType = Byte.parseByte(args[1]);
        n = Integer.parseInt(args[2]);

        switch (algorithmId) {
            case ALG_FIBONACCI:
                printFibonacci(loopType, n);
                break;

            case ALG_FACTORIAL:
                printFactorial(loopType, n);
                break;

            default:
                System.out.println("Invalid input");
        }
    }

    static void printFibonacci(byte loopType, int n) {

        switch (loopType) {
            case LOOP_WHILE:
                printFibonacciWithWhile(n);
                break;

            case LOOP_DO_WHILE:
                printFibonacciWithDoWhile(n);
                break;

            case LOOP_FOR:
                printFibonacciWithFor(n);
                break;

            default:
                System.out.println("Invalid input");
        }
    }

    private static void printFibonacciWithWhile(int n) {
        int num1 = 1;
        int num2 = 1;
        int i = 1;

        while (i <= n) {
            System.out.print(num1 + " ");
            int sumOfPrevTwo = num1 + num2;
            num1 = num2;
            num2 = sumOfPrevTwo;
            i++;
        }
    }

    private static void printFibonacciWithDoWhile(int n) {
        int num1 = 1;
        int num2 = 0;
        int i = 0;

        do {
            int sumOfPrevTwo = num1 + num2;
            num1 = num2;
            num2 = sumOfPrevTwo;
            System.out.print(sumOfPrevTwo + " ");
            i++;
        } while (i < n);
    }

    private static void printFibonacciWithFor(int n) {
        int num1 = 1;
        int num2 = 1;
        int sumOfPrevTwo;

        for (int i = 1; i <= n; i++) {
            System.out.print(num1 + " ");
            sumOfPrevTwo = num1 + num2;
            num1 = num2;
            num2 = sumOfPrevTwo;
        }
    }

    private static void printFactorial(byte loopType, int n) {
        switch (loopType) {
            case LOOP_WHILE:
                System.out.println("Factorial of " + n + " is: " + calculateFactorialWithWhile(n));
                break;

            case LOOP_DO_WHILE:
                System.out.println("Factorial of " + n + " is: " + calculateFactorialWithDoWhile(n));
                break;

            case LOOP_FOR:
                System.out.println("Factorial of " + n + " is: " + calculateFactorialWithFor(n));
                break;

            default:
                System.out.println("Invalid input");
        }
    }

    private static long calculateFactorialWithWhile(int n) {
        long fact = 1;
        int i = 1;

        while (i <= n) {
            fact *= i;
            i++;
        }

        return fact;
    }

    private static long calculateFactorialWithDoWhile(int n) {
        long fact = 1;
        int i = 1;

        do {
            fact *= i;
            i++;
        } while (i <= n);

        return fact;
    }

    private static long calculateFactorialWithFor(int n) {
        long fact = 1;

        for (int i = 1; i <= n; i++) {
            fact *= i;
        }

        return fact;
    }
}