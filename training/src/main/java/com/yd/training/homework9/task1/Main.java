package com.yd.training.homework9.task1;

import java.sql.*;
import java.util.Scanner;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        EmployeeManager manager = new EmployeeManager();
        manager.openConnection();
        boolean repeat = true;
        Scanner reader = new Scanner(System.in);
        while (repeat) {
            System.out.println("Enter a command\n1 - Add an employee;\n2 - Delete an employee;\n3 - Print employees table;\n4 - Exit.");
            int command = reader.nextInt();
            switch (command) {
                case 1:
                    addEmployee(manager, reader);
                    break;
                case 2:
                    removeEmployee(manager, reader);
                    break;
                case 3:
                    printEmployeeTable(manager);
                    break;
                case 4:
                    repeat = false;
                    break;
                default:
                    System.out.println("Invalid input.");
            }
        }
        manager.closeConnection();
        reader.close();
    }

    private static void addEmployee(EmployeeManager manager, Scanner reader) {
        System.out.println("Enter employee first name:");
        String firstName = reader.next();
        System.out.println("Enter employee last name:");
        String lastName = reader.next();
        Employee employee = new Employee(0, firstName, lastName);
        try {
            manager.addEmployee(employee);
        } catch (Exception e) {
            System.out.println(String.format("Cannot add employee to the table."));
        }
    }

    private static void removeEmployee(EmployeeManager manager, Scanner reader) {
        System.out.println("Enter employee ID to remove:");
        int employeeId = reader.nextInt();
        if (manager.removeEmployee(employeeId)) {
            System.out.println(String.format("Employee with ID: %d was successfully removed.", employeeId));
        } else {
            System.out.println(String.format("Employee with ID: %d doesn't exist.", employeeId));
        }
    }

    private static void printEmployeeTable(EmployeeManager manager) {
        for (Employee employee : manager.getEmployees()) {
            System.out.println(employee);
        }
    }
}

class EmployeeManager {

    private static final String connectionString = "jdbc:sqlite:src/main/resources/db/db_for_m4_l3.db";
    private Connection connection;

    public void openConnection() {
        try {
            Class.forName("org.sqlite.JDBC");
            this.connection = DriverManager.getConnection(connectionString);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void closeConnection() {
        try {
            if (this.connection != null && !this.connection.isClosed()) {
                this.connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean addEmployee(Employee employee) {
        boolean result = false;
        try {
            Statement statement = connection.createStatement();
            String sql = String.format(
                    "INSERT INTO Employees (firstname, lastname) values ('%s','%s')",
                    employee.getFirstName(),
                    employee.getLastName());
            statement.executeUpdate(sql);
            result = true;
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean removeEmployee(Integer id) {
        boolean result = false;
        try {
            Statement statement = connection.createStatement();
            String sql = String.format("DELETE FROM Employees WHERE id = %d", id);
            statement.executeUpdate(sql);
            result = true;
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<Employee> getEmployees() {
        ArrayList<Employee> result = new ArrayList<Employee>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from Employees");
            while (resultSet.next()) {
                Integer id = resultSet.getInt("id");
                String firstname = resultSet.getString(2);
                String lastname = resultSet.getString(3);
                result.add(new Employee(id, firstname, lastname));
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}

