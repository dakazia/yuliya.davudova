package com.yd.training.homework5.task1;

import java.util.*;

public class Sorting {

    public static void main(String[] args) {
        String text = "Once upon a time a Wolf was lapping at a spring on a hillside, when, looking up, what should he see but Lamb just beginning to drink a little lower down";
        List<String> words = new LinkedList<String>(Arrays.asList(text.toLowerCase().split("[\\s\\p{Punct}]+")));
        List<String> uniqueWords = new ArrayList<String>(new LinkedHashSet<String>(words));
        Collections.sort(uniqueWords);
        String letter = uniqueWords.get(0).substring(0, 1);
        boolean redline = true;
        for (String word : uniqueWords) {
            if (!word.substring(0, 1).equals(letter)) {
                redline = true;
                letter = word.substring(0, 1);
            }
            if (redline) {
                System.out.print("\n" + letter.toUpperCase() + ": " + "\t" + word);
            } else {
                System.out.print(", " + word);
            }
            redline = false;
        }
    }
}


