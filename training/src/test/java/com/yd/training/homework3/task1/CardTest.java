package com.yd.training.homework3.task1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class CardTest {

    private Card card;

    @Before
    public void setUp() {
        card = new Card("Mr.Smith", 500);
    }

    @Test
    public void testDepositCash() {
        Assert.assertEquals(600, card.depositCash(100), 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDepositCash_NegativeBalance() {
        card.depositCash(-123);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDepositCashZero() {
        card.depositCash(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithdrawCashZero() {
        card.withdrawCash(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithdrawCash_NegativeBalance() {
        card.withdrawCash(-123);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithdrawCash_Greater() {
        card.withdrawCash(600);
    }

    @Test
    public void testWithdrawCash() {
        Assert.assertEquals(400, card.withdrawCash(100), 0);
    }

    @Test
    public void testConvertBalance() {
        Assert.assertEquals(1200, card.convertBalance(2.4), 0);
    }
}