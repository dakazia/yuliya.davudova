package com.yd.training.homework4.task1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

public class AtmTest {

    private static Card card;

    @Before
    public void setUp(){
        card = new Card("Mr.Smith", 500);
    }
    @After
    public void tearDown() { card = null; }

    @Test
    public void testInsertCard() {
        Assert.assertNotNull("Card already inserted.", card != null);
    }

    @Test
    public void removeCard() {
        Assert.assertFalse("There is no card in this ATM. Please, insert card first.", card == null);
    }

    @Test
    public void testDepositCash() {
        Assert.assertEquals(600, card.depositCash(100), 0);
    }

    @Test
    public void withdrawCash() {
        Assert.assertEquals(400, card.withdrawCash(100), 0);
    }
}