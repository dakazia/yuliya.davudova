package com.yd.training.homework4.task1;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class DebitCardTest {

    private static DebitCard card;

    @BeforeClass
    public static void setUp(){
        card = new DebitCard("Mr.Smith", 500);
    }

    @Test
    public void testWithdrawCash() {
        double withdrawAmount = 600;
        Assert.assertTrue("Withdraw amount cannot be greater than available balance.", withdrawAmount > card.balance);
    }
}