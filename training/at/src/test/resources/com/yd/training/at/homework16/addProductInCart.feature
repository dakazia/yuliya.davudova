@great
Feature: Add the product in the cart

  Scenario: Put the goods in the cart
    Given Ebay home page is opened
    When I search the product "Core Java"
    Then I navigate to product page
    Then I add the product to the cart
    Then the product "Core Java" is in the cart