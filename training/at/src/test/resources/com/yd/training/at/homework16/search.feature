@great
Feature: Search product

  Scenario Outline: Running a Full Text Quick Search
    Given Ebay home page is opened
    When I search the product "<request>"
    Then the term query "<request>" should be the first in the Search Result grid

    Examples:
      | request       |
      | Apple Airpods |
      | Core Java     |

