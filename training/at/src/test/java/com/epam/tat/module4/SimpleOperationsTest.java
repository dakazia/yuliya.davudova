package com.epam.tat.module4;

import org.testng.Assert;
import org.testng.annotations.*;

public class SimpleOperationsTest {

    private Calculator calculator;

    @BeforeClass
    public void setUp() {
        calculator = new Calculator();
    }

    @AfterClass
    public void tearDown() {
        calculator = null;
    }

    @DataProvider
    public static Object[][] getValuesDouble() {
        return new Object[][]{
                {1.8, 3},
                {0, 0},
                {11.55, -10.55},
                {-5, -6.7},
                {526589752896458.52d, 9220002585654775807d}
        };
    }

    @DataProvider
    public static Object[][] getValuesLong() {
        return new Object[][]{
                {1, 3},
                {0, 0},
                {11528465255874522L, -1055},
                {-5, -6700122558654L},
                {990998, 1},
                {526589752896458L, 9220002585654775807L}
        };
    }

    @Test(dataProvider = "getValuesLong")
    public void testSumL(long a, long b) {
        System.out.println("a: " + a + " b: " + b);
        Assert.assertEquals(calculator.sum(a, b), a + b);
    }

    @Test(dataProvider = "getValuesDouble", dependsOnMethods = "testSumL", alwaysRun = true)
    public void testSumD(double a, double b) {
        System.out.println("a: " + a + " b: " + b);
        Assert.assertEquals(calculator.sum(a, b), a + b);
    }

    @Test(dataProvider = "getValuesDouble",dependsOnMethods = "testSubL", alwaysRun = true)
    public void testSubD(double a, double b) {
        System.out.println("a: " + a + " b: " + b);
        Assert.assertEquals(calculator.sub(a, b), a - b);
    }

    @Test(dataProvider = "getValuesLong")
    public void testSubL(long a, long b) {
        System.out.println("a: " + a + " b: " + b);
        Assert.assertEquals(calculator.sub(a, b), a - b);
    }

    @Test(dataProvider = "getValuesLong", groups = "Mult")
    public void testMultL(long a, long b) {
        System.out.println("a: " + a + " b: " + b);
        Assert.assertEquals(calculator.mult(a, b), a * b);
    }

    @Test(dataProvider = "getValuesDouble", groups = "Mult")
    public void testMultD(double a, double b) {
        System.out.println("a: " + a + " b: " + b);
        Assert.assertEquals(calculator.mult(a, b), a * b);
    }

    @Test(dataProvider = "getValuesLong", dependsOnGroups = "Mult")
    public void testDivL(long a, long b) {
        System.out.println("a: " + a + " b: " + b);
        if (b == 0L) {
            throw new NumberFormatException("Attempt to divide by zero");
        } else {
            Assert.assertEquals(calculator.div(a, b), a / b);
        }
    }

    @Test(dataProvider = "getValuesDouble")
    public void testDivD(double a, double b) {
        System.out.println("a: " + a + " b: " + b);
        if ((a / b) == Double.POSITIVE_INFINITY || (a / b) == Double.NEGATIVE_INFINITY) {
            throw new NumberFormatException();
        } else {
            Assert.assertEquals(calculator.div(a, b), a / b);
        }
    }
}