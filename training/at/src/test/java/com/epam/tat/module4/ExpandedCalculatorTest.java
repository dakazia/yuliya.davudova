package com.epam.tat.module4;

import org.testng.Assert;
import org.testng.annotations.*;

public class ExpandedCalculatorTest {

    private Calculator calculator;
    private double a;
    private double b;

    @BeforeClass
    public void setUp() {
        calculator = new Calculator();
    }

    @AfterClass
    public void tearDown() {
        calculator = null;
    }

    @DataProvider
    public static Object[][] getValues() {
        return new Object[][]{
                {1, 3},
                {0, 0},
                {11.55, -10.55},
                {-5, -6.7},
                {990998, 1},
                {526589752896458.52d, 9220002585654775807d}
        };
    }

    @Factory(dataProvider = "getValues")
    public ExpandedCalculatorTest(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Test
    public void testPow() {
        System.out.println("a: " + a + " b: " + b );
        Assert.assertEquals(Math.pow(a, b), calculator.pow(a, b));
    }

    @Test
    public void testSqrt()  throws ArithmeticException {
        System.out.println("a: " + a + " b: " + b );
        if (a < 0) {
            Assert.assertEquals(calculator.sqrt(a),"Error");
        } else {
            Assert.assertEquals(calculator.sqrt(a), Math.sqrt(a));
        }
    }

    @Test
    public void testTg() {
        System.out.println("a: " + a );
            Assert.assertEquals(calculator.tg(a),Math.sin(a)/Math.cos(a));
    }

    @Test
    public void testCtg() {
        System.out.println("a: " + a );
        Assert.assertEquals(calculator.ctg(a),1/Math.tan(a));
    }

    @Test
    public void testCos() {
        System.out.println("a: " + a );
        Assert.assertEquals(calculator.cos(a), Math.cos(a));
    }

    @Test
    public void testSin() {
        System.out.println("a: " + a );
        Assert.assertEquals(calculator.sin(a), Math.sin(a));
    }
}
