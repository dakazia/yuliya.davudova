package com.epam.tat.module4;

import org.testng.annotations.*;
import static org.testng.Assert.*;

public class BooleanExpressions {

    private Calculator calculator;
    private long val;

    @BeforeClass
    public void setUp() {
        calculator = new Calculator();
    }

    @AfterClass
    public void tearDown() {
        calculator = null;
    }

    @DataProvider
    public static Object[][] getValues() {
        return new Object[][]{
                {-5},
                {0},
                {2256256855654585585L},
        };
    }

    @Factory(dataProvider = "getValues")
    public BooleanExpressions(long val) {
        this.val= val;
    }

    @Test
    public void testIsPositive (){
        System.out.println("val: " + val);
        if (val == 0L) {
            assertFalse(calculator.isPositive(val));
        } if (val > 0L){
            assertTrue(calculator.isPositive(val));
        }
    }


    @Test
    public void testIsNegative (){
        System.out.println("val: " + val);
        if (val < 0L) {
            assertTrue(calculator.isNegative(val));
        }
    }
}


