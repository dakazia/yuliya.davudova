package com.yd.training.at.homework13.actions.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

public class DialogPage extends Page {

    private static final By DIALOG_PAGE = By.xpath(".//*[@id='sidebar']//a[contains(@href,'dialog')]");
    private static final By DEMO_FRAME = By.className("demo-frame");
    private static final By DIALOG_WINDOW = By.xpath("//div[@role='dialog']");
    private static final By DIALOG_WINDOW_HEAD = By.className("ui-dialog-title");
    private static final By RESIZE_WINDOW_ELEMENT = By.xpath("//div[contains(@class,'ui-icon-gripsmall-diagonal-se')]");
    private static final By CLOSE_WINDOW_ELEMENT = By.xpath("//span[contains(@class,'ui-icon-closethick')]");
    private static final By VISIBLE_DIALOG_WINDOW = By.xpath("//body//div[contains(@style,'display: none')and @role='dialog']");

    private Actions actions = new Actions(driver);

    private Point locationBeforeMoving;
    private Point locationAfterMoving;
    private Dimension sizeElementBeforeResize;
    private Dimension sizeElementAfterResize;

    public DialogPage(WebDriver driver) {
        super(driver);
    }

    private void dragDialogWindow() {
        WebElement dialogElement = driver.findElement(DIALOG_WINDOW_HEAD);
        locationBeforeMoving = dialogElement.getLocation();
        actions.dragAndDropBy(dialogElement, -100, -100)
                .build()
                .perform();
        locationAfterMoving = dialogElement.getLocation();

    }

    private void resizeDialogWindow() {
        WebElement resizeElement = driver.findElement(RESIZE_WINDOW_ELEMENT);
        sizeElementBeforeResize = driver.findElement(DIALOG_WINDOW).getSize();
        actions.clickAndHold(resizeElement);
        actions.moveByOffset(50, 50)
                .release()
                .build()
                .perform();
        sizeElementAfterResize = driver.findElement(DIALOG_WINDOW).getSize();
    }

    private void closeDialogWindow() {
        WebElement closeElement = driver.findElement(CLOSE_WINDOW_ELEMENT);
        actions.click(closeElement)
                .build()
                .perform();
    }

    public DialogPage useDialog() {
        driver.findElement(DIALOG_PAGE).click();
        driver.switchTo().frame(driver.findElement(DEMO_FRAME));
        dragDialogWindow();
        resizeDialogWindow();
        closeDialogWindow();
        return this;
    }

    public boolean isDialogWindowOpen() {
        return driver.findElement(VISIBLE_DIALOG_WINDOW).isDisplayed();
    }

    public boolean isDialogWindowDraged() {
        return locationAfterMoving != locationBeforeMoving;
    }

    public boolean isDialogWindowResized() {
        return sizeElementBeforeResize != sizeElementAfterResize;
    }
}
