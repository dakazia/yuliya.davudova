package com.yd.training.at.homework16.pages;

import org.openqa.selenium.By;

public class CartPage extends Page{

    private static final String CART_PAGE = "https://cart.ebay.com/";
    private final By nameOfProductInCart =By.xpath ("//div[@class='cart-bucket']//h3");

    public String getProductNameInCart() {
        waitForUrlContains(CART_PAGE);
        return driver.findElement(nameOfProductInCart).getText();
    }
}
