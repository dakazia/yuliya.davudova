package com.yd.training.at.homework12.pageFactory.tests;

import com.yd.training.at.homework12.pageFactory.pages.*;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GmailTest extends ConfigurationTest {

    private static final String USER_NAME = "testnight01";
    private static final String PASSWORD = "testemap0106@";
    private static final String EMAIL_ADDRESS = "dyx.afi@gmail.com";
    private static final String INDEX_OF_EMAIL = Long.toString(System.currentTimeMillis());
    private static final String TEXT_MESSAGE = "Good morning!";
    protected static final String LOG_OFF_URL_ELEMENT = "signin";
    private static final By EMAIL_ADDRESS_ELEMENT = By.xpath("//div[@role='region']//span[(@email) and not(@class)]");
    private static final By EMAIL_SUBJECT_ELEMENT = By.xpath("//h2[@class='a3E']");
    private static final By EMAIL_MESSAGE_ELEMENT = By.xpath("//div[@role='textbox']");


    @Test(priority = 1)
    public void loginTest() {
        InboxPage inboxPage = new LoginPage(driver).loginToAccount(USER_NAME, PASSWORD);
        Assert.assertTrue(inboxPage.getTextFromCurrentUrl().contains(LoginPage.INBOX_PAGE_URL_ELEMENT));
    }

    @Test(priority = 2)
    public void saveEmailInDraftTest() {
        DraftPage draftPage = new InboxPage(driver).createDraft(EMAIL_ADDRESS, INDEX_OF_EMAIL, TEXT_MESSAGE);
        Assert.assertTrue(draftPage.getAllSubjectDraft(INDEX_OF_EMAIL));
    }

    @Test(priority = 3)
    public void emailAddressOfDraftTest() {
        Assert.assertTrue(driver.findElement(EMAIL_ADDRESS_ELEMENT).getText().contains(EMAIL_ADDRESS));
    }

    @Test(priority = 3)
    public void emailSubjectOfDraftTest() {
        System.out.println(INDEX_OF_EMAIL);
        Assert.assertTrue(driver.findElement(EMAIL_SUBJECT_ELEMENT).getText().contains(INDEX_OF_EMAIL));
    }

    @Test(priority = 3)
    public void emailTextMessageOfDraftTest() {
        Assert.assertTrue(driver.findElement(EMAIL_MESSAGE_ELEMENT).getText().contains(TEXT_MESSAGE));
    }

    @Test(priority = 4)
    public void emailOutFromDraftPageTest() {
        DraftPage draftPage = new DraftPage(driver);
        Assert.assertFalse(draftPage.sendEmail(INDEX_OF_EMAIL));
    }

    @Test(priority = 5)
    public void mailIsInSentPageTest() {
        SentPage sendMail = new DraftPage(driver).openSentPage();
        Assert.assertTrue (sendMail.getAllSubjectSentEmail(INDEX_OF_EMAIL));
    }

    @Test(priority = 6)
    public void logOutTest() {
        NavigationMenu logOut = new NavigationMenu(driver).logOffFromAccount(LOG_OFF_URL_ELEMENT);
        Assert.assertTrue(logOut.getTextFromCurrentUrlLogOff().contains("signin"));
    }
}


