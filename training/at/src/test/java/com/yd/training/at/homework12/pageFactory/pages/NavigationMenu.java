package com.yd.training.at.homework12.pageFactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class NavigationMenu extends Page {

    @FindBy(xpath = "//a[contains(@href, 'SignOutOptions')]")
    private WebElement userAccountElement;

    @FindBy(id = "gb_71")
    private WebElement logOggElement;

    public NavigationMenu(WebDriver driver) {
        super(driver);
    }

    protected NavigationMenu openPage (WebElement element) {
        element.click();
        return this;
    }

    public NavigationMenu logOffFromAccount(String urlElement) {
        userAccountElement.click();
        waitForElementVisibility(userAccountElement);
        logOggElement.click();
        waitForUrlContains(urlElement);
        return this;
    }
    public String getTextFromCurrentUrlLogOff() {
        return driver.getCurrentUrl();
    }
}

