package com.yd.training.at.homework13.actions.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class DroppablePage extends Page {

    private static final By DROPPABLE_PAGE = By.xpath(".//*[@id='sidebar']//a[contains(@href,'droppable')]");
    private static final By DEMO_FRAME = By.className("demo-frame");
    private static final By DROP_ELEMENT = By.id("draggable");
    private static final By DROPPABLE_ELEMENT = By.id("droppable");

    public DroppablePage(WebDriver driver) {
        super(driver);
    }

    public DroppablePage moveSquare() {
        driver.findElement(DROPPABLE_PAGE).click();
        driver.switchTo().frame(driver.findElement(DEMO_FRAME));
        WebElement dropElement = driver.findElement(DROP_ELEMENT);
        Actions actions = new Actions(driver);
        actions.dragAndDropBy(dropElement, 170, 50)
                .build()
                .perform();
        return new DroppablePage(driver);
    }

    public boolean isElementDroppabled() {
        return moveSquare().driver.findElement(DROPPABLE_ELEMENT).getText().contains("Dropped!");
    }
}
