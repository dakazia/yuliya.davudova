package com.yd.training.at.homework15.service;

public class MessageCreator {

    private  static MessageCreatorInf messageCreator;

    public static String creator(String text) {
        switch (text) {
            case "Good": {
                messageCreator = new GoodMessageCreatorInf();
                break;
            }
            case "Bad": {
                messageCreator = new BadMessageCreatorInf();
                break;
            }
            default: {
                messageCreator = new GoodMessageCreatorInf();
            }
        }

        Message message = messageCreator.createMessage();
        return message.getMessage();
    }
}
