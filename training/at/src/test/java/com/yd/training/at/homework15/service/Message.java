package com.yd.training.at.homework15.service;

public abstract class Message {

    private String message;

    public Message(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
