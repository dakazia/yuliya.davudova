package com.yd.training.at.homework15.pages;

import com.yd.training.at.homework15.utils.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class DraftPage extends NavigationMenu {

    private static final By DRAFT_SUBJECT_LIST = By.xpath("//div[@role='main']//tbody/tr");
    private static final By SEND_EMAIL_ELEMENT = By.xpath("//div[@class='dC']//div[@role='button'][1]");
    private static final String DRAFT_COMPOSE_URL_ELEMENT = "compose";
    private static final By DRAFT_NEW_SUBJECT_LIST = By.xpath("//div[@role='main']//tbody/tr");
    private static final By EMAIL_ADDRESS_ELEMENT = By.xpath("//div[@role='region']//div[@tabindex='1']//span[@email]"); //"//div[@class='aoD hl']//span[@email]");
    private static final By EMAIL_SUBJECT_ELEMENT = By.xpath("//h2[@class='a3E']");
    private static final By EMAIL_MESSAGE_ELEMENT = By.xpath("//div[@role='textbox']");
    private static final String DRAFT_PAGE_URL_ELEMENT = "#draft";

    public DraftPage(WebDriver driver) {
        super(driver);
    }

    public Boolean openLastDraft(String indexOfEmail) {
        openDraftPage();
        waitForUrlContains(DRAFT_PAGE_URL_ELEMENT);
        List<WebElement> draftList = driver.findElements(DRAFT_SUBJECT_LIST);
        for (int i = 0; i < draftList.size(); i++) {
            if (draftList.get(i).getText().contains(indexOfEmail)) {
                WebElement currentDraft = draftList.get(i);
                currentDraft.click();
                return true;
            }
        }
        return false;
    }

    public boolean isLastDraftByDefault() {
        return openLastDraft(Utils.getDefaultTestLetter().getSubject());
    }

    public boolean isEmailAddressOfDraftByDefault() {
        return driver.findElement(EMAIL_ADDRESS_ELEMENT)
                .getText().contains(Utils.getDefaultTestLetter().getEmailAddress());
    }

    public boolean isEmailSubjectOfDraftByDefault() {
        return driver.findElement(EMAIL_SUBJECT_ELEMENT)
                .getText().contains(Utils.getDefaultTestLetter().getSubject());
    }

    public boolean isEmailTextMessageOfDraftByDefault() {
        return driver.findElement(EMAIL_MESSAGE_ELEMENT)
                .getText().contains(Utils.getDefaultTestLetter().getTextMessage());
    }

    public boolean isEmailOutFromDraftPage() {
        return sendEmail(Utils.getDefaultTestLetter().getSubject());
    }

    private Boolean sendEmail(String indexOfEmail) {
        driver.findElement(SEND_EMAIL_ELEMENT).click();
        waitForUrlNotContains(DRAFT_COMPOSE_URL_ELEMENT);
        List<WebElement> newDraftList = driver.findElements(DRAFT_NEW_SUBJECT_LIST);
        for (int i = 0; i < newDraftList.size(); i++) {
            if (newDraftList.get(i).getText().contains(indexOfEmail)) {
                return true;
            }
        }
        return false;
    }
}
