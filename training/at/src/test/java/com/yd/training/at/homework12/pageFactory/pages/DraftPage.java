package com.yd.training.at.homework12.pageFactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class DraftPage extends NavigationMenu {
    private static final String DRAFT_PAGE_URL_ELEMENT = "#draft";
    private static final String DRAFT_COMPOSE_URL_ELEMENT = "compose";
    private static final String SENT_URL_ELEMENT = "#sent";

    @FindBy (xpath = "//a[contains(@href,'drafts')]")
    private  WebElement draftPage;

    @FindBy (xpath ="//div[@role='main']//tbody/tr" )
    protected  List<WebElement> subjectList;

    @FindBy (xpath = "//div[@class='dC']//div[@role='button'][1]")
    private WebElement sendEmailElement;

    @FindBy (xpath = "//a[contains(@href,'#sent')]")
    private WebElement sentPage;

    public DraftPage(WebDriver driver) {
        super(driver);
    }

    public Boolean getAllSubjectDraft(String indexOfEmail) {
        openPage(draftPage);
        waitForUrlContains(DRAFT_PAGE_URL_ELEMENT);
        List<WebElement> draftList = subjectList;
        boolean result = false;
        for (int i = 0; i < draftList.size(); i++) {
            if (draftList.get(i).getText().contains(indexOfEmail)) {
                WebElement currentDraft = draftList.get(i);
                currentDraft.click();
                result = true;
                break;
            }
        }
        return result;
    }

    public Boolean sendEmail(String indexOfEmail) {
        sendEmailElement.click();
        waitForUrlNotContains(DRAFT_COMPOSE_URL_ELEMENT);
        List<WebElement> newDraftList = subjectList;
        boolean result = false;
        for (int i = 0; i < newDraftList.size(); i++) {
            if (newDraftList.get(i).getText().contains(indexOfEmail)) {
                result=true;
            }
        }
        return result;
    }

    public SentPage openSentPage() {
        openPage(sentPage);
        waitForUrlContains(SENT_URL_ELEMENT);
        return new SentPage(driver);
    }
}
