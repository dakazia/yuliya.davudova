package com.yd.training.at.homework12.pageFactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class InboxPage extends NavigationMenu {

    @FindBy(xpath = "//textarea[@role='combobox']")
    private WebElement emailAddressElement;

    @FindBy(xpath = "//input[@name='subjectbox']")
    private WebElement emailSubjectElement;

    @FindBy(xpath = "//div[@role='textbox']")
    private WebElement emailMessageElement;

    @FindBy(xpath = "//div[@class='nH bkL']//div[@role='button']")
    private WebElement composeEmailElement;

    @FindBy(xpath = "//img[@class='Ha']")
    private WebElement saveDraftElement;

    public InboxPage(WebDriver driver) {
        super(driver);
    }

    public String getTextFromCurrentUrl() {
        return driver.getCurrentUrl();
    }

    private void setEmailAddressElement(String email) {
        emailAddressElement.sendKeys(email);
    }

    private void setSubjectEmailElement (String indexOfEmail) {
        emailSubjectElement.sendKeys(indexOfEmail);
    }

    private void setEmailMessageElement(String textMessage) {
        emailMessageElement.sendKeys(textMessage);
    }

    public DraftPage createDraft(String email, String indexOfEmail, String textMessage) {
        waitForElementClickable(composeEmailElement);
        composeEmailElement.click();
        setEmailAddressElement(email);
        setSubjectEmailElement(indexOfEmail);
        setEmailMessageElement(textMessage);
        saveDraftElement.click();
        return new DraftPage(driver);
    }
}


