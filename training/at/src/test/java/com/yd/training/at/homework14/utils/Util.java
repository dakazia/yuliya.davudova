package com.yd.training.at.homework14.utils;

import com.yd.training.at.homework14.models.Letter;
import com.yd.training.at.homework14.models.User;

public class Util {

    private static final String EMAIL_ADDRESS = "dyx.afi@gmail.com";
    private static final String TEXT_MESSAGE = "Good morning!";
    public static final String INDEX_OF_EMAIL = Long.toString(System.currentTimeMillis());
    private static final String USER_NAME = "testnight01";
    private static final String USER_PASSWORD = "testemap0106@";

    public static User getDefaultTestUser() {
        return new User(USER_NAME, USER_PASSWORD);
    }

    public static Letter getDefaultTestLetter() {
        return new Letter(EMAIL_ADDRESS, Util.INDEX_OF_EMAIL, TEXT_MESSAGE);
    }

}

