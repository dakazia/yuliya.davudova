package com.yd.training.at.homework16.steps;

import com.yd.training.at.homework16.pages.CartPage;
import com.yd.training.at.homework16.pages.HomePage;
import com.yd.training.at.homework16.pages.ProductPage;
import com.yd.training.at.homework16.pages.ResultPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class OrderOnEbayDef {
    @Given("^Ebay home page is opened$")
    public void iOpenedEBayPage() {
        new HomePage().open();
    }

    @When("^I search the product \"([^\"]*)\"$")
    public void iSearchTheProduct(String query) {
        new HomePage().enterQuery(query).clickSubmit();
    }

    @Then("^the term query \"([^\"]*)\" should be the first in the Search Result grid$")
    public void theTermQueryShouldBeTheFirstInTheSearchResultGrid(String expectPhrase) {
        assertThat(new ResultPage().getTextFromFirstProduct(), containsString(expectPhrase));
    }

    @Then("^I navigate to product page$")
    public void iNavigateToProductPage() {
        new ResultPage().clickOnFirstProduct();
        assertTrue(new ProductPage().isPageOpened());

    }

    @Then("^I add the product to the cart$")
    public void iAddProductToTheCart() {
        new ProductPage().addProductInCart();
    }

    @Then("^the product \"([^\"]*)\" is in the cart$")
    public void theProductIsInTheCart(String expectPhrase) {
        assertThat(new CartPage().getProductNameInCart(), containsString(expectPhrase));
    }
}
