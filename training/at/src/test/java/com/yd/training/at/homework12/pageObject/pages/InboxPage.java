package com.yd.training.at.homework12.pageObject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class InboxPage extends NavigationMenu {

    private static final By EMAIL_ADDRESS_ELEMENT= By.xpath("//textarea[@role='combobox']");
    private static final By EMAIL_SUBJECT_ELEMENT = By.xpath("//input[@name='subjectbox']");
    private static final By EMAIL_MESSAGE_ELEMENT = By.xpath("//div[@role='textbox']");
    private static final By COMPOSE_EMAIL_ELEMENT = By.xpath("//div[@class='nH bkL']//div[@role='button']");
    private static final By SAVE_DRAFT_ELEMENT = By.xpath("//img[@class='Ha']");

    public InboxPage(WebDriver driver) {
        super(driver);
    }

    public String getTextFromCurrentUrl() {
        return driver.getCurrentUrl();
    }

    private void setEmailAddressElement(String email){
        driver.findElement (EMAIL_ADDRESS_ELEMENT).sendKeys(email);
    }

    private void setSubjectEmailElement(String indexOfEmail){
        driver.findElement(EMAIL_SUBJECT_ELEMENT).sendKeys(indexOfEmail);
    }

    private void setEmailMessageElement(String textMessage){
        driver.findElement(EMAIL_MESSAGE_ELEMENT).sendKeys(textMessage);
    }

    public DraftPage createDraft (String email, String indexOfEmail, String textMessage) {
        waitForElementClickable (COMPOSE_EMAIL_ELEMENT);
        driver.findElement(COMPOSE_EMAIL_ELEMENT).click();
        setEmailAddressElement (email);
        setSubjectEmailElement(indexOfEmail);
        setEmailMessageElement(textMessage);
        driver.findElement(SAVE_DRAFT_ELEMENT).click();
        return new DraftPage(driver);
    }
}
