package com.yd.training.at.homework14.tests;

import com.yd.training.at.homework14.models.Letter;
import com.yd.training.at.homework14.models.User;
import com.yd.training.at.homework14.pages.*;
import com.yd.training.at.homework14.utils.Util;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GmailTest extends ConfigurationTest {

    @Test(priority = 1)
    public void loginTest() {
        User testUser = Util.getDefaultTestUser();
        InboxPage inboxPage = new LoginPage(driver).loginToAccount(testUser);
        Assert.assertTrue(inboxPage.isInboxPageOpened());
    }

    @Test(priority = 2)
    public void saveEmailInDraftTest() {
        Letter testLetter = Util.getDefaultTestLetter();
        DraftPage draftPage = new InboxPage(driver).createDraft(testLetter);
        Assert.assertTrue(draftPage.isLastDraftByDefault());
    }

    @Test(priority = 3)
    public void emailAddressOfDraftTest() {
        Assert.assertTrue(new DraftPage(driver).isEmailAddressOfDraftByDefault());
    }

    @Test(priority = 3)
    public void emailSubjectOfDraftTest() {
        Assert.assertTrue(new DraftPage(driver).isEmailSubjectOfDraftByDefault());
    }

    @Test(priority = 3)
    public void emailTextMessageOfDraftTest() {
        Assert.assertTrue(new DraftPage(driver).isEmailTextMessageOfDraftByDefault());
    }

    @Test(priority = 4)
    public void emailOutFromDraftPageTest() {
        Assert.assertFalse(new DraftPage(driver).isEmailOutFromDraftPage());
    }

    @Test(priority = 5)
    public void mailIsInSentPageTest() {
        SentPage sendMail = new DraftPage(driver).openSentPage();
        Assert.assertTrue(sendMail.isEmailInSentPage());
    }

    @Test(priority = 6)
    public void logOutTest() {
        NavigationMenu logOffFromAccount = new NavigationMenu(driver).logOffFromAccount();
        Assert.assertTrue(logOffFromAccount.isLogOut());
    }

}

