package com.yd.training.at.homework12.pageObject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.List;

public class SentPage extends NavigationMenu {

    private static final By SENT_NEW_SUBJECT_LIST = By.xpath("//div[@role='main']//tbody/tr");

    public SentPage(WebDriver driver) {
        super(driver);
    }

    public Boolean getAllSubjectSentEmail(String indexOfEmail) {
        List<WebElement> sentList = driver.findElements(SENT_NEW_SUBJECT_LIST);
        boolean result = false;
        for (int i = 0; i < sentList.size(); i++) {
            if (sentList.get (i).getText().contains(indexOfEmail)) {
                result=true;
            }
        }
        return result;
    }
}
