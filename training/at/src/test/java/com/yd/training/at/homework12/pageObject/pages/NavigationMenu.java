package com.yd.training.at.homework12.pageObject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class NavigationMenu extends Page {

    protected static final String LOG_OFF_URL_ELEMENT = "signin";
    protected static final By USER_ACCOUNT_ELEMENT = By.xpath("//a[contains(@href, 'SignOutOptions')]");
    protected static final By LOG_OFF_ELEMENT = By.id("gb_71");

    public NavigationMenu(WebDriver driver) {
        super(driver);
    }

    protected NavigationMenu openPage(By locator) {
        driver.findElement(locator).click();
        return this;
    }

    public NavigationMenu logOffFromAccount( String urlElement) {
        driver.findElement (USER_ACCOUNT_ELEMENT).click();
        waitForElementVisibility(LOG_OFF_ELEMENT);
        driver.findElement(LOG_OFF_ELEMENT).click();
        waitForUrlContains(urlElement);
        return this;
    }

    public String getTextFromCurrentUrlLogOff() {
        return driver.getCurrentUrl();
    }
}

