package com.yd.training.at.homework12.pageFactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SentPage extends NavigationMenu {

    @FindBy(xpath ="//div[@role='main']//tbody/tr" )
    private   List<WebElement> subjectList;

    public SentPage(WebDriver driver) {
        super(driver);
    }

    public Boolean getAllSubjectSentEmail (String indexOfEmail) {
        List<WebElement> sentList = subjectList;
        boolean result = false;
        for (int i = 0; i < sentList.size(); i++) {
            if (sentList.get(i).getText().contains(indexOfEmail)) {
                result=true;
            }
        }
        return result;
    }
}
