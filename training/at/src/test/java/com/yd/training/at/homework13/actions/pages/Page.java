package com.yd.training.at.homework13.actions.pages;

import org.openqa.selenium.WebDriver;

public abstract class Page {

    public final WebDriver driver;

    protected Page(WebDriver driver) {
        this.driver = driver;
    }
}
