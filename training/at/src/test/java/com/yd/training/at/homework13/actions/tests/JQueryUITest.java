package com.yd.training.at.homework13.actions.tests;

import com.yd.training.at.homework13.actions.pages.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class JQueryUITest extends BaseTest {

    @Test
    public void dropTest() {
        DroppablePage droppablePage = new DroppablePage(driver);
        Assert.assertTrue(droppablePage.isElementDroppabled());
        driver.switchTo().defaultContent();
    }

    @Test
    public void checkBoxRadioTest() {
        CheckBoxRadioPage checkBoxRadioPage = new CheckBoxRadioPage(driver).setCheckBoxRadio();
        Assert.assertTrue(checkBoxRadioPage.isNewYorkButtonSelected());
        Assert.assertTrue(checkBoxRadioPage.isTwoStarButtonSelected());
        driver.switchTo().defaultContent();
    }

    @Test
    public void selectMenuTest() {
        SelectMenuPage selectMenuPage = new SelectMenuPage(driver).setSelectMenu();
        Assert.assertTrue(selectMenuPage.isValueFastSelected());
        Assert.assertTrue(selectMenuPage.isValueNumberSelected());
        driver.switchTo().defaultContent();
    }

    @Test
    public void DialogTest() {
        DialogPage dialogPage = new DialogPage(driver).useDialog();
        Assert.assertTrue(dialogPage.isDialogWindowResized());
        Assert.assertTrue(dialogPage.isDialogWindowDraged());
        Assert.assertFalse(dialogPage.isDialogWindowOpen());
        driver.switchTo().defaultContent();
    }

    @Test
    public void toolTipTest() {
        TooltipPage tooltip = new TooltipPage(driver).getTooltip();
        Assert.assertEquals(tooltip.actualTooltipFirstLink, "That's what this widget is");
        Assert.assertEquals(tooltip.actualTooltipSecondLink, "ThemeRoller: jQuery UI's theme builder application");
        Assert.assertEquals(tooltip.actualTooltip, "We ask for your age only for statistical purposes.");
        driver.switchTo().defaultContent();
    }
}