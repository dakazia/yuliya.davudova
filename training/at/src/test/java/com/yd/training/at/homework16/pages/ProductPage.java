package com.yd.training.at.homework16.pages;

import com.yd.training.at.homework16.browser.Browser;
import org.openqa.selenium.By;

public class ProductPage extends Page {

    private static final String PRODUCT_PAGE = "https://www.ebay.com/itm";
    private final By addToCart =By.id ("isCartBtn_btn");

    public boolean isPageOpened() {
        return Browser.getCurrentUrl().contains(PRODUCT_PAGE);
    }

    public CartPage addProductInCart() {
        waitForElementVisible(addToCart);
        driver.findElement(addToCart).click();
        return new CartPage();
    }
}
