package com.yd.training.at.homework14.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class NavigationMenu extends Page {

    private static final By USER_ACCOUNT_ELEMENT = By.xpath("//a[contains(@href, 'SignOutOptions')]");
    private static final By LOG_OFF_ELEMENT = By.id("gb_71");
    private static final String SENT_URL_ELEMENT = "#sent";
    private static final By SENT_PAGE = By.xpath("//a[contains(@href,'#sent')]");
    private static final By DRAFT_PAGE = By.xpath("//a[contains(@href,'drafts')]");
    private static final String DRAFT_PAGE_URL_ELEMENT = "#draft";
    private static final String LOG_OFF_URL_ELEMENT = "signin";

    public NavigationMenu(WebDriver driver) {
        super(driver);
    }

    private NavigationMenu openPage(By locator) {
        driver.findElement(locator).click();
        return this;
    }

    public NavigationMenu logOffFromAccount() {
        driver.findElement(USER_ACCOUNT_ELEMENT).click();
        waitForElementVisibility(LOG_OFF_ELEMENT);
        driver.findElement(LOG_OFF_ELEMENT).click();
        waitForUrlContains(LOG_OFF_URL_ELEMENT);
        return this;
    }

    public boolean isLogOut (){
        return getTextFromCurrentUrlLogOff().contains(LOG_OFF_URL_ELEMENT);
    }

    public SentPage openSentPage() {
        openPage(SENT_PAGE);
        waitForUrlContains(SENT_URL_ELEMENT);
        return new SentPage(driver);
    }

    public DraftPage openDraftPage() {
    openPage(DRAFT_PAGE);
    waitForUrlContains(DRAFT_PAGE_URL_ELEMENT);
        return new DraftPage(driver);
    }
}
