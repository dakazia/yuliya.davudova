package com.yd.training.at.homework15.service;

public class BadMessageCreatorInf implements MessageCreatorInf {

    public Message createMessage() {
        return new BadMessage();
    }
}
