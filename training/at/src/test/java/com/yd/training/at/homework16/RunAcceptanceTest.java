package com.yd.training.at.homework16;

import com.yd.training.at.homework16.browser.Browser;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        plugin = {
                "pretty", "json:target/Cucumber.json",
                "html:target/cucumber-html-report"
        }, tags = {"@great"}
)
public class RunAcceptanceTest {

    @AfterClass
    public static void closeDriver() {
        Browser.closeDriver();
    }
}