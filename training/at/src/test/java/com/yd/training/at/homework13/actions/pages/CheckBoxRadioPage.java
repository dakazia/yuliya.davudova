package com.yd.training.at.homework13.actions.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class CheckBoxRadioPage extends Page {

    private static final By CHECKBOXRADIO_PAGE = By.xpath(".//*[@id='sidebar']//a[contains(@href,'checkboxradio')]");
    private static final By DEMO_FRAME = By.className("demo-frame");
    private static final By NEW_YORK_BUTTON = By.xpath("//label[@for='radio-1']//span");
    private static final By TWO_STAR_BUTTON = By.xpath("//label[@for='checkbox-1']//span");
    private static final By RADIO_ELEMENT = By.xpath("//label[@for='radio-1']");
    private static final By CHECK_ELEMENT = By.xpath("//label[@for='checkbox-1']");

    private Actions actions = new Actions(driver);

    public CheckBoxRadioPage(WebDriver driver) {
        super(driver);
    }

    private void setNewYorkButton() {
        WebElement newYorkButton = driver.findElement(NEW_YORK_BUTTON);
        actions.click(newYorkButton)
                .build()
                .perform();
    }

    private void setTwoStarButton() {
        WebElement twoStarButton = driver.findElement(TWO_STAR_BUTTON);
        actions.click(twoStarButton)
                .build()
                .perform();
    }

    public CheckBoxRadioPage setCheckBoxRadio() {
        driver.findElement(CHECKBOXRADIO_PAGE).click();
        driver.switchTo().frame(driver.findElement(DEMO_FRAME));
        setNewYorkButton();
        setTwoStarButton();
        return this;
    }

    public boolean isNewYorkButtonSelected() {
        return driver.findElement(RADIO_ELEMENT)
                .getAttribute("class").contains("checked");
    }

    public boolean isTwoStarButtonSelected() {
        return driver.findElement(CHECK_ELEMENT)
                .getAttribute("class").contains("checked");
    }
}
