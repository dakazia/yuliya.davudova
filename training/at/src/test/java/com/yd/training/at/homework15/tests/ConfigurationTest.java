package com.yd.training.at.homework15.tests;

import com.yd.training.at.homework15.driver.DriverSingleton;
import com.yd.training.at.homework15.utils.Listener;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;

import java.util.concurrent.TimeUnit;

@Listeners({Listener.class})
public class ConfigurationTest {

    protected static WebDriver driver;

    private static final String TEST_URL = "https://www.gmail.com/";

    @BeforeClass(alwaysRun = true)
    public static void setUp() {
        driver = DriverSingleton.getDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(TEST_URL);
    }

    @AfterClass(alwaysRun = true)
    public static void tearDown() {
        DriverSingleton.closeDriver();
    }
}

