package com.yd.training.at.homework12.pageFactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class Page {
    protected final WebDriver driver;

    public Page(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    protected void waitForElementVisibility (WebElement element) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(element));
    }

    protected void waitForUrlContains(String fraction) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.urlContains(fraction));
    }

    protected void waitForElementClickable(WebElement element) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(element));
    }

    protected void waitForUrlNotContains(String fraction) {
        new WebDriverWait(driver, 20).until(ExpectedConditions.not(ExpectedConditions.urlContains(fraction)));
    }
}