package com.yd.training.at.homework16.pages;

import com.yd.training.at.homework16.browser.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class Page {

    protected WebDriver driver = Browser.getDriver();

    public void waitForElementVisible(By locator){
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
    protected void waitForUrlContains(String fraction) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.urlContains(fraction));
    }
}
