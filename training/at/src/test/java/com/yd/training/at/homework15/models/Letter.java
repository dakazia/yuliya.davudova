package com.yd.training.at.homework15.models;

import java.util.Objects;

public class Letter {

    private String emailAddress;
    private String subject;
    private String textMessage;

    public Letter(String emailAddress, String subject, String textMessage) {
        this.emailAddress = emailAddress;
        this.subject = subject;
        this.textMessage = textMessage;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTextMessage() {
        return textMessage;
    }

    public void setTextMessage(String textMessage) {
        this.textMessage = textMessage;
    }

    @Override
    public String toString() {
        return "Letter{" +
                "emailAddress='" + emailAddress + '\'' +
                ", subject='" + subject + '\'' +
                ", testMessage='" + textMessage +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Letter)) return false;
        Letter letter = (Letter) o;
        return Objects.equals(getEmailAddress(), letter.getEmailAddress()) &&
                Objects.equals(getSubject(), letter.getSubject()) &&
                Objects.equals(getTextMessage(), letter.getTextMessage());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEmailAddress(), getSubject(), getTextMessage());
    }
}
