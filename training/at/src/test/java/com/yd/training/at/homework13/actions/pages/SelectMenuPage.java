package com.yd.training.at.homework13.actions.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class SelectMenuPage extends Page {

    private static final By SELECTMENU_PAGE = By.xpath(".//*[@id='sidebar']//a[contains(@href,'selectmenu')]");
    private static final By DEMO_FRAME = By.className("demo-frame");
    private static final By SPEED_DROP_DOWN_ELEMENT = By.id("speed-button");
    private static final By VALUE_FAST_ELEMENT = By.xpath("//div[contains(text(),'Fast')]");
    private static final By NUMBER_DROP_DOWN_ELEMENT = By.id("number-button");
    private static final By VALUE_THREE_ELEMENT = By.xpath("//ul[@id='number-menu']//div[contains(text(),'3')]");
    private static final By FIELD_SET_ELEMENT = By.tagName("fieldset");

    private Actions actions = new Actions(driver);

    public SelectMenuPage(WebDriver driver) {
        super(driver);
    }

    private void selectSpeedDropDownList() {
        WebElement speedDropDownList = driver.findElement(SPEED_DROP_DOWN_ELEMENT);
        actions.moveToElement(speedDropDownList)
                .click(speedDropDownList)
                .build()
                .perform();
    }

    private void selectValueFast() {
        WebElement valueFast = driver.findElement(VALUE_FAST_ELEMENT);
        actions.moveToElement(valueFast)
                .click(valueFast)
                .build()
                .perform();
    }

    private void selectNumberDropDownList() {
        WebElement numberDropDownList = driver.findElement(NUMBER_DROP_DOWN_ELEMENT);
        actions.moveToElement(numberDropDownList)
                .click(numberDropDownList)
                .build()
                .perform();
    }

    private void selectValueNumber() {
        WebElement valueThree = driver.findElement(VALUE_THREE_ELEMENT);
        actions.moveToElement(valueThree)
                .click(valueThree)
                .build()
                .perform();
    }

    public SelectMenuPage setSelectMenu() {
        driver.findElement(SELECTMENU_PAGE).click();
        driver.switchTo().frame(driver.findElement(DEMO_FRAME));
        selectSpeedDropDownList();
        selectValueFast();
        selectNumberDropDownList();
        selectValueNumber();
        return this;
    }

    public boolean isValueFastSelected() {
        return driver.findElement(FIELD_SET_ELEMENT).getText().contains("Fast");
    }

    public boolean isValueNumberSelected() {
        return driver.findElement(FIELD_SET_ELEMENT).getText().contains("3");
    }
}
