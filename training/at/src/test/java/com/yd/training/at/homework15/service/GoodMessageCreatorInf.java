package com.yd.training.at.homework15.service;

public class GoodMessageCreatorInf implements MessageCreatorInf {

    public Message createMessage() {
        return new GoodMessage();
    }
}
