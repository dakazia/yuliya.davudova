package com.yd.training.at.homework13.actions.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class TooltipPage extends Page {

    private static final By TOOLTIP_PAGE = By.xpath(".//*[@id='sidebar']//a[contains(@href,'tooltip')]");
    private static final By DEMO_FRAME = By.className("demo-frame");
    private static final By FIRST_LINK_ELEMENT = By.xpath("//a[contains(text(),'Tooltips')]");
    private static final By LINK_TOOLTIP = By.xpath("//div[@role='log']//div[not(@style)]");
    private static final By SECOND_LINK_ELEMENT = By.xpath("//a[contains(text(),'ThemeRoller')]");
    private static final By INPUT_BOX_ELEMENT = By.xpath("//input[@id='age']");
    private static final By TOOLTIP_INPUT = By.xpath("//div[contains(text(),'We')]");

    public TooltipPage(WebDriver driver) {
        super(driver);
    }

    private Actions actions = new Actions(driver);

    public String actualTooltipFirstLink;
    public String actualTooltipSecondLink;
    public String actualTooltip;

    private void getTooltipFirstLink() {
        WebElement firstLink = driver.findElement(FIRST_LINK_ELEMENT);
        actions.moveToElement(firstLink)
                .build()
                .perform();
        WebElement tooltipFirstLinkElement = driver.findElement(LINK_TOOLTIP);
        actualTooltipFirstLink = tooltipFirstLinkElement.getText();
    }

    private void getTooltipSecondLink() {
        WebElement secondLink = driver.findElement(SECOND_LINK_ELEMENT);
        actions.moveToElement(secondLink)
                .build()
                .perform();
        WebElement tooltipSecondLinkElement = driver.findElement(LINK_TOOLTIP);
        actualTooltipSecondLink = tooltipSecondLinkElement.getText();
    }

    private void getTooltipInput() {
        WebElement inputElement = driver.findElement(INPUT_BOX_ELEMENT);
        actions.clickAndHold(inputElement)
                .build()
                .perform();
        WebElement tooltipInputElement = driver.findElement(TOOLTIP_INPUT);
        actualTooltip = tooltipInputElement.getText();
    }

    public TooltipPage getTooltip() {
        driver.findElement(TOOLTIP_PAGE).click();
        driver.switchTo().frame(driver.findElement(DEMO_FRAME));
        getTooltipFirstLink();
        getTooltipSecondLink();
        getTooltipInput();
        return this;
    }
}
