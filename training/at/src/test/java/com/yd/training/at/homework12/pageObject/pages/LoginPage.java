package com.yd.training.at.homework12.pageObject.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;

public class LoginPage extends Page {

    private static final By USER_ELEMENT = By.id("identifierId");
    private static final By IDENTIFIER_NEXT_BUTTON = By.id("identifierNext");
    private static final By PASSWORD_ELEMENT = By.name("password");
    private static final By SIGN_IN_BUTTON = By.id("passwordNext");
    public static final String INBOX_PAGE_URL_ELEMENT = "#inbox";

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    private void setUserName(String userName) {
        driver.findElement (USER_ELEMENT).sendKeys(userName);
        driver.findElement(IDENTIFIER_NEXT_BUTTON).click();
    }

    private void setPassword(String password) {
        driver.findElement(PASSWORD_ELEMENT).sendKeys(password);
        driver.findElement(SIGN_IN_BUTTON).click();
    }

    public InboxPage loginToAccount(String userName, String password) {
        setUserName(userName);
        setPassword(password);
        waitForUrlContains(INBOX_PAGE_URL_ELEMENT);
        return new InboxPage(driver);
    }
}

