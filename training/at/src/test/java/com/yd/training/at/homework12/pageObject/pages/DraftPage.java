package com.yd.training.at.homework12.pageObject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.List;

public class DraftPage extends NavigationMenu {

    private static final By DRAFT_PAGE = By.xpath("//a[contains(@href,'drafts')]");
    private static final String DRAFT_PAGE_URL_ELEMENT = "#draft";
    private static final By DRAFT_SUBJECT_LIST = By.xpath("//div[@role='main']//tbody/tr");
    private static final By SEND_EMAIL_ELEMENT = By.xpath("//div[@class='dC']//div[@role='button'][1]");
    private static final String DRAFT_COMPOSE_URL_ELEMENT = "compose";
    private static final String SENT_URL_ELEMENT = "#sent";
    private static final By SENT_PAGE = By.xpath("//a[contains(@href,'#sent')]");
    private static final By DRAFT_NEW_SUBJECT_LIST = By.xpath("//div[@role='main']//tbody/tr");

    public DraftPage(WebDriver driver) {
        super(driver);
    }

    public Boolean getAllSubjectDraft(String indexOfEmail) {
        openPage(DRAFT_PAGE);
        waitForUrlContains(DRAFT_PAGE_URL_ELEMENT);
        List<WebElement> draftList = driver.findElements(DRAFT_SUBJECT_LIST);
        boolean result = false;
        for (int i = 0; i < draftList.size(); i++) {
            if (draftList.get(i).getText().contains(indexOfEmail)) {
                WebElement currentDraft = draftList.get(i);
                currentDraft.click();
                result = true;
                break;
            }
        }
        return result;
    }

    public Boolean sendEmail(String indexOfEmail) {
        driver.findElement(SEND_EMAIL_ELEMENT).click();
        waitForUrlNotContains(DRAFT_COMPOSE_URL_ELEMENT);
        List<WebElement> newDraftList = driver.findElements(DRAFT_NEW_SUBJECT_LIST);
        boolean result = false;
        for (int i = 0; i < newDraftList.size(); i++) {
            if (newDraftList.get(i).getText().contains(indexOfEmail)) {
                result=true;
            }
        }
        return result;
    }

    public SentPage openSentPage() {
        openPage(SENT_PAGE);
        waitForUrlContains(SENT_URL_ELEMENT);
        return new SentPage(driver);
     }
}