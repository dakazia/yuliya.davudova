package com.yd.training.at.homework12.pageFactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends Page {

    public static final String INBOX_PAGE_URL_ELEMENT = "#inbox";

    @FindBy(id = "identifierId")
    private WebElement userElement;

    @FindBy(id = "identifierNext")
    private WebElement identifierNextButton;

    @FindBy(name = "password")
    private WebElement passwordElement;

    @FindBy(id = "passwordNext")
    private WebElement signInButton;

    public LoginPage (WebDriver driver) {
        super(driver);
    }

    private void setUserName(String userName) {
        userElement.sendKeys(userName);
        identifierNextButton.click();
    }

    private void setPassword(String password) {
        passwordElement.sendKeys(password);
        signInButton.click();
    }

    public InboxPage loginToAccount(String userName, String password) {
        setUserName(userName);
        setPassword(password);
        waitForUrlContains(INBOX_PAGE_URL_ELEMENT);
        return new InboxPage(driver);
    }
}


